import { Router } from "express";

let productRouter=Router();

productRouter.
route("/")
.post((req,res)=>{
    console.log(req.body)
    res.json("product post")            //res.json jaile last ma rakhni

})
.get((req,res)=>{
    res.json("second get")
})
.patch((req,res)=>{
    res.json("second patch")
})
.delete((req,res)=>{
    res.json("second delete")
})

productRouter
.route("/:id")
.post()
.get((req,res)=>{
    console.log(req.params)
    console.log(req.query)
    res.json("i will show the detail page")
})
export default productRouter;