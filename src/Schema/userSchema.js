import { Schema } from "mongoose";

//define the content of schema
let userSchema=Schema({
    name:{type:String},
    age:{type:Number},
    isMarried:{type:Boolean},
});

export default userSchema