import mongoose from "mongoose";

let connectDb=async ()=>{
    let dbPort="mongodb://0.0.0.0:27017/manish";
    try{
        await mongoose.connect(dbPort);
        console.log(`application is connected to mongodb at port ${dbPort} successfully`);
    }
    catch(error){
        console.log(error.message);
    }
}
export default connectDb