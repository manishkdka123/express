import { Router } from "express";

let superAdmin=Router();
superAdmin.
route("/super")
.post((req,res)=>{
    res.json("superadmin post")
})
.get((req,res)=>{
    res.json("superadmin get")
})
.patch((req,res)=>{
    res.json("superadmin patch")
})
.delete((req,res)=>{
    res.json("superadmin delete")
})
export default superAdmin;