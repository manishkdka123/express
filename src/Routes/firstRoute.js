import { Router } from "express";
import { HttpStatus } from "../config/constant.js";

let firstRouter = Router()

// defining Route
firstRouter
.route("/admin")
.get((req,res)=>{
    res.send("I am admin method get")
    res.status(HttpStatus.OK).json("get successfully");
   
})
.post((req,res)=>{
    res.json("I am admin method post")
})
.patch((req,res)=>{
    res.json("I am admin method patch")
})
.delete((req,res)=>{
    res.json("I am admin method delete")
})

firstRouter
.route("/admin/name")
.post((req,res)=>{
    res.json("I am admin main method post")
})
.get((req,res)=>{
    res.json("I am admin main method get")
})
.patch((req,res)=>{
    res.json("I am admin main method patch")
})
.delete((req,res)=>{
    res.json("I am admin main method delete")
})
export default firstRouter