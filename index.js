import express, { json } from "express";
import firstRouter from "./src/Routes/firstRoute.js";
import productRouter from "./productRoute.js";
import superAdmin from "./src/Routes/superAdmin.js";
import connectDb from "./src/connectdb/connectdb.js";
import User from "./src/Schema/model.js";

// Using the route to index.js
let app = express()
let port = 8000
app.use(json())                 //yesko matlab hamro express app le json data lina sakxa vanya
connectDb();                    //connecting to database mongodb
let createUser=async()=>{
    let data={
        name:"manish",
        age:"22",
        isMarried:false,
    };
    try{
        let result=await User.create(data);
        console.log(result);
    }
    catch(error){
        console.log(error.message)
    }
}
createUser()
app.use((req,res,next)=>{
    console.log("I am first middleware")
    next()
}, 
(req,res,next)=>{
    console.log("i am second middleware")
    next()
}
)
 
app.use("/a",firstRouter)
app.use("/product",productRouter)
app.use("/",superAdmin)
app.use((err,req,res,next)=>{
console.log("i am error middleware")
next(false)
})
app.use((err,req,res,next)=>{
    console.log("i am second error middleware")
    
    })
app.listen(port,()=>{console.log(`app is listening at port ${port}`)})

